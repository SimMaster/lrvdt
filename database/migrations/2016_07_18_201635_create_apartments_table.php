<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('apartments', function (Blueprint $table) {
            $table->increments('a_id');

            $table->string('a_name');

            $table->float('a_price')->default(0);

            $table->integer('a_bedroom_c')->default(0);
            $table->integer('a_bathroom_c')->default(0);
            $table->integer('a_storey_c')->default(0);
            $table->integer('a_garage_c')->default(0);

            $table->dateTime('a_created_at');
            $table->dateTime('a_updated_at');

            // For MyISAM
            //$table->index('a_name', 'i_a_name');

            $table->index('a_price', 'i_a_price');

            $table->index('a_bedroom_c', 'i_a_bedroom_c');
            $table->index('a_bathroom_c', 'i_a_bathroom_c');
            $table->index('a_storey_c', 'i_a_storey_c');
            $table->index('a_garage_c', 'i_a_garage_c');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('apartments');
    }
}
