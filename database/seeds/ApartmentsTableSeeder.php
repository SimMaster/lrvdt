<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use League\Csv\Reader;

class ApartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('apartments')->truncate();
        //FOR POSTGRESQL
        //DB::table('apartments')->select(DB::raw("setval('apartments_prf_id_seq', 0)"))->get();
        $file = base_path().'/database/seeds/csvs/property-data.csv';
        $csv = Reader::createFromPath($file);
        $intIndex = 0;
        $nbInsert = $csv->each(function ($row, $currentIterator, $iteratorKey) {
            // return false if there is no data
            if ( empty( $row ) ) {
                return TRUE;
            }

            if( $currentIterator == 0) {
                return TRUE;
            }

            return DB::table( 'apartments' )->insert(
                array(

                    'a_name'        => $row[0],
                    'a_price'       => $row[1],

                    'a_bedroom_c'   => $row[2],
                    'a_bathroom_c'  => $row[3],
                    'a_storey_c'    => $row[4],
                    'a_garage_c'    => $row[5],


                    'a_created_at'  => Carbon::now()->toDateTimeString(),
                    'a_updated_at'  => Carbon::now()->toDateTimeString(),

                )
            );
        });
    }
}
