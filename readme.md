## Demo Project - Laravel 5.1


## Requirements
Composer, 
DB: MySQL
PHP: 5.6

## Deploy

Please use composer, migration and seeds


### License

The Project is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
