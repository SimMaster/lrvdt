$(function () {
   var getData = function(params){
       return $.ajax({
           type: 'POST',
           url: urSearch,
           data: params.data,
           success: function (data, status, jqXHR) {
               showInTable(data);
           },
           error: function (data, status, jqXHR) {
               showInTable(null);
           }
       })
           

   };

   $('#js_search').click(function () {
       var form = $(this).closest('form');

       var dataForm = {};
       if( $('#name').val() != '' ) {
           dataForm.name = $('#name').val();
       }

       if( $('#price_from').val() != '' ) {
           dataForm.price_from = $('#price_from').val();
       }
       if( $('#price_to').val() != '' ) {
           dataForm.price_to = $('#price_to').val();
       }
       if( $('#bedroom').val() != '' ) {
           dataForm.bedroom = $('#bedroom').val();
       }
       if( $('#bathroom').val() != '' ) {
           dataForm.bathroom = $('#bathroom').val();
       }
       if( $('#storey').val() != '' ) {
           dataForm.storey = $('#storey').val();
       }
       if( $('#garage').val() != '' ) {
           dataForm.garage = $('#garage').val();
       }


       getData({data:dataForm});
   })

   var getAllData = function () {
       var data = getData({data: {name: ' '}});

       console.log(data);
   };
   var createItem = function (data) {
     var strLines = [];
     $.each(data, function (field, strValue) {
         var objItem = $('<div>');
         objItem.addClass('row');

         var objColumn = $('<div>');
         objColumn.addClass('col-lg-12');
         objColumn.addClass('bg-info');
         objColumn.addClass('text-left');

         var objLabel = $('<label>');
         objLabel.addClass('inline-block');
         objLabel.html(field);
         var objData = $('<div>');
         objData.addClass('inline-block');
         objData.html(strValue);
         objColumn.append(objLabel);
         objColumn.append(objData);
         objItem.append(objColumn);
         strLines.push(objItem);
     }) ;


     return strLines;
   };

   var showInTable = function (data) {
       var objWrapper = $('.jsWrapperData');
       var objAlertNoData = $('.js-alert-no-data-view');
       objAlertNoData.hide();
       objWrapper.html('');
        console.log(data);
       if( data ) {

           if (data.hasOwnProperty('data')) {
               var objItem = $('<div>');
               objItem.addClass("row");
               $.each(data.data, function (index, objAprt) {
                   var objColumn = $('<div>');
                   objColumn.addClass('col-lg-3');
                   objColumn.addClass('col-lg-3 item-block');
                   objColumn.append(createItem(objAprt));
                   objItem.append(objColumn);
                   objWrapper.append(objItem);
               });
           } else {
               objAlertNoData.show();
           }
       } else {
           objAlertNoData.show();
       }


   };

   getAllData();
});