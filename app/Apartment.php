<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{

    const CREATED_AT = 'a_created_at';
    const UPDATED_AT = 'a_updated_at';

    protected $primaryKey = 'a_id';
    protected $table = 'apartments';

    protected $fillable = [
        'a_name',
        'a_price',
        'a_bedroom_c',
        'a_bathroom_c',
        'a_storey_c',
        'a_garage_c',


    ];

    protected $dates = [ 'a_created_at', 'a_updated_at'];

    protected $_fPrefix = 'a_';




    protected $casts = [
        'a_id' => 'integer',
        'a_name' => 'string',
        'a_price' => 'float',
        'a_bedroom_c' => 'integer',
        'a_bathroom_c' => 'integer',
        'a_storey_c' => 'integer',
        'a_garage_c' => 'integer',


        'a_created_at' => 'datetime',
        'a_updated_at' => 'datetime',
    ];

    /**
     *
     * Get object by ID
     * @param int $intId
     *
     * @return bool
     */
    public function getById($intId) {

        $objItem = $this->find($intId);
        if( !$objItem ) {
            return false;
        } else {
            return $objItem;
        }
    }

    /**
     * Create object
     * @param $arrData
     * @return bool|static
     */
    public function createItem($arrData) {
        if( !is_array($arrData) || empty($arrData)) {
            return false;
        }

        $arrPrepareData = $this->_mapBeforeSave($arrData);
        if( !$arrPrepareData ) {
            return false;
        }

        return $this->create($arrPrepareData);
    }

    /**
     * Prepare input data before save (mapping)
     * @param array $arrData
     * @return array|bool
     */
    protected function _mapBeforeSave($arrData) {
        if( !isset($arrData['name'])
            || !isset($arrData['price'])


        ) {
            return false;
        }

        $arrOut = [];
        foreach ($arrData as $keyField => $strValue) {
            $strFieldName = ($this->_fPrefix . $keyField);
            if( $this->_issetFieldName($strFieldName) ) {
                $arrOut[($strFieldName)] = $strValue;
            }
        }

        if( empty($arrOut) ) {
            return false;
        }


        return $arrOut;
    }

    /**
     * Check isset field name
     * @param $strFieldName
     * @return bool
     */
    private function _issetFieldName($strFieldName)
    {
        if( in_array($strFieldName, $this->fillable) ) {
            return true;
        }
        return false;
    }


    /**
     * Prepare data before output
     * @return array
     */
    public function mapSimple()
    {

        $arrOut = [
            'id' => $this->{$this->_fPrefix.'id'},
            'name' => $this->{$this->_fPrefix.'name'},
            'price' => $this->{$this->_fPrefix.'price'},
            'bedroom' => $this->{$this->_fPrefix.'bedroom_c'},
            'bathroom' => $this->{$this->_fPrefix.'bathroom_c'},
            'storey' => $this->{$this->_fPrefix.'storey_c'},
            'garage' => $this->{$this->_fPrefix.'garage_c'},


        ];

        return $arrOut;
    }


    /**
     * @param $addDataForUpdate
     * @return bool
     */
    public function mapBeforeUpdate($addDataForUpdate)
    {
        $arrOut = [];
        foreach ($addDataForUpdate as $keyField => $strValue) {
            $strFieldName = ($this->_fPrefix . $keyField);
            if( $this->_issetFieldName($strFieldName) ) {
                $arrOut[($strFieldName)] = $strValue;
            }
        }

        if( empty($arrOut) ) {
            return false;
        }

    }

    /**
     * @param $arrData
     * @return bool
     */
    public function searchByData($arrData)
    {
        $objRequest = $this->select();


        if( isset($arrData['name']) ) {
            $objRequest->where(($this->_fPrefix.'name'), 'LIKE', '%' . $arrData['name'] . '%');
        }
        if( isset($arrData['price']['from'])  ) {
            $objRequest->where(($this->_fPrefix.'price'), '>=', $arrData['price']['from']);
        }

        if( isset($arrData['price']['to'])  ) {
            $objRequest->where(($this->_fPrefix.'price'), '<=', $arrData['price']['to']);
        }

        if( isset($arrData['bedroom'])  ) {
            $objRequest->where(($this->_fPrefix.'bedroom_c'), '=', $arrData['bedroom']);
        }
        if( isset($arrData['bathroom'])  ) {
            $objRequest->where(($this->_fPrefix.'bathroom_c'), '=', $arrData['bathroom']);
        }
        if( isset($arrData['storey'])  ) {
            $objRequest->where(($this->_fPrefix.'storey_c'), '=', $arrData['storey']);
        }
        if( isset($arrData['garage'])  ) {
            $objRequest->where(($this->_fPrefix.'garage_c'), '=', $arrData['garage']);
        }


        $listItems = $objRequest->get();

        if( !$listItems ) {
            return false;
        }

        return $listItems;
    }
}
