<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class SearchController extends ApiController
{
    /**
     * Search items method
     *
     *
     */
    public function index(Request $request)
    {
        $allFormData = $request->all();
        $validator = Validator::make($allFormData, [
            'name' => 'string',
            'price_from' => 'numeric',
            'price_to' => 'numeric',
            'bedroom' => 'integer',
            'bathroom' => 'integer',
            'storey' => 'integer',
            'garage' => 'integer',


        ]);
        $arrDataOut = [];

        if (!$validator->fails()) {
            $tblApartments = new Apartment();
            $arrData = [];
            //Prepare input for search
            $arrData = $this->_prepareDataForSearch($allFormData);



            if( !empty($arrData) ) {

                $listApartment = $tblApartments->searchByData($arrData);

                if ($listApartment) {
                     foreach ($listApartment as $keyIndex  => $objApartment) {
                         /**
                          * @var Apartment $objApartment
                          */
                         $arrDataOut[] = $objApartment->mapSimple();
                    }

                }
            }
        } else {
            $messages = $validator->errors();
            $arrErrors = [];
            foreach ($messages->all() as $message) {
                $arrErrors[] = [
                    'code' => null,
                    'message' => $message
                ];
            }


            if (!empty($arrErrors)) {
                $success = false;
                return $this->sendAnswer( [], '', $success, $arrErrors, $request );
            }
        }


        return $this->sendAnswer( $arrDataOut, '', TRUE, [], $request );
    }

    private function _prepareDataForSearch($allFormData)
    {

        $arrOut = [];

        if( isset($allFormData['name']) ) {
            $strName = $allFormData['name'];
            if( !empty($strName) ) {
                $arrOut['name'] = $strName;
            }
        }
        if( isset($allFormData['price_from']) ) {
            $fltPriceFrom = floatval($allFormData['price_from']);
            $arrOut['price']['from'] = $fltPriceFrom;
        }
        if( isset($allFormData['price_to']) ) {
            $fltPriceTo = floatval($allFormData['price_to']);
            $arrOut['price']['to'] = $fltPriceTo;
        }
        if( isset($allFormData['bedroom']) ) {
            $intBedroom = (int) $allFormData['bedroom'];
            $arrOut['bedroom'] = $intBedroom;
        }
        if( isset($allFormData['bathroom']) ) {
            $intBathroom = (int) $allFormData['bathroom'];
            $arrOut['bathroom'] = $intBathroom;
        }
        if( isset($allFormData['storey']) ) {
            $intStorey = (int) $allFormData['storey'];
            $arrOut['storey'] = $intStorey;
        }
        if( isset($allFormData['garage']) ) {
            $intGarage = (int) $allFormData['garage'];
            $arrOut['garage'] = $intGarage;
        }
        return $arrOut;
    }


}
