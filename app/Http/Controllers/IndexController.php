<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class IndexController extends ApiController
{
    /**
     * Search items method
     *
     *
     */
    public function index(Request $request)
    {

        return $this->sendAnswer( [], 'welcome', TRUE, [], $request );
    }



}
