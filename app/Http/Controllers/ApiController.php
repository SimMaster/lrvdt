<?php namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller {

	protected function _stripTags($data)
	{
		if( is_array($data) ) {
			foreach( $data as $keyField => $inData ) {
				if( !is_array($inData) ) {
					$data[$keyField] = strip_tags($inData);
				}
			}
		} else {
			$data = strip_tags($data);
		}

		return $data;
	}

	/**
	 * @param array $data
	 * @param string $template
	 * @param bool $success
	 * @param array $error
	 * @param \Illuminate\Http\Request|NULL $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
	 */
	public function sendAnswer($data , $template = '', $success = true, $error = [], Request $request = null, $cookie = null)
	{
		$arrAnswer = [];
		$arrAnswer['success'] = $success;
		if( $error ) {
			$arrAnswer['error'] = $error;
		}

		if( !empty($data) ) {
			$arrAnswer['data'] = $data;
		}


		if ( $request ) {
			/**
			 * @var Request $request
			 */
			if( $request->header('content-type') == 'application/json' ) {
				if( !empty($cookie)) {
					$strCookieName = $cookie['name'];
					$strCookieValue = $cookie['value'];
					if( isset($cookie['forget']) ) {
						return response()->json( $arrAnswer )->withCookie(cookie()->forget($strCookieName));
					}
					return response()->json( $arrAnswer )->withCookie(cookie()->forever($strCookieName, $strCookieValue, null, null, false, false));
				} else {
					return response()->json( $arrAnswer );
				}
			} if ( !empty( $template ) ) {
				if( !empty($cookie)) {
					$strCookieName = $cookie['name'];
					$strCookieValue = $cookie['value'];
					if( isset($cookie['forget']) ) {
						return view( $template, $arrAnswer )->withCookie(cookie()->forget($strCookieName));
					}
					return view( $template, $arrAnswer )->withCookie(cookie()->forever($strCookieName, $strCookieValue, null, null, false, false));
				} else {
					return view( $template, $arrAnswer );
				}
			} else {
				if( !empty($cookie)) {
					$strCookieName = $cookie['name'];
					$strCookieValue = $cookie['value'];
					if( isset($cookie['forget']) ) {
						return response()->json( $arrAnswer )->withCookie(cookie()->forget($strCookieName));
					}
					return response()->json( $arrAnswer )->withCookie(cookie()->forever($strCookieName, $strCookieValue, null, null, false, false));
				} else {
					return response()->json( $arrAnswer );
				}
			}

		} else {
			if ( empty( $template ) ) {
				if( !empty($cookie)) {
					$strCookieName = $cookie['name'];
					$strCookieValue = $cookie['value'];
					if( isset($cookie['forget']) ) {
						return response()->json( $arrAnswer )->withCookie(cookie()->forget($strCookieName));
					}
					return response()->json( $arrAnswer )->withCookie(cookie()->forever($strCookieName, $strCookieValue, null, null, false, false));
				} else {
					return response()->json( $arrAnswer );
				}
			} else {
				if( !empty($cookie)) {
					$strCookieName = $cookie['name'];
					$strCookieValue = $cookie['value'];
					if( isset($cookie['forget']) ) {
						return view( $template, $arrAnswer )->withCookie(cookie()->forget($strCookieName));
					}
					return view( $template, $arrAnswer )->withCookie(cookie()->forever($strCookieName, $strCookieValue, null, null, false, false));
				} else {
					return view( $template, $arrAnswer );
				}
			}
		}
	}




}