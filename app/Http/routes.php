<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [ 'as' => 'index_api', 'uses' => 'IndexController@index']);

Route::group(['prefix' => 'api/v1'], function()
{
    Route::post('/search', [ 'as' => 'search_api', 'uses' => 'SearchController@index']);

});